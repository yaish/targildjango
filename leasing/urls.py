# -*- coding: utf-8 -*-
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^price-list-for-car/(?P<string>.+)/$', views.price_list_for_car, name= 'price_list_for_car'),
    url(r'^price-list-for-company/(?P<string>.+)/$', views.price_list_for_company, name= 'price_list_for_company'),
    url(r'^$', views.index, name='index'),
    url(r'^all-cars-from-active-companies/$', views.all_cars_from_active_companies, name= 'all_cars_from_active_companies'),
    url(r'^active_contains_yud/$', views.active_contains_yud, name= 'active_contains_yud')

]