# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse
from .models import Car, Company,PriceList
from django.utils.encoding import python_2_unicode_compatible
import pandas as pd
import openpyxl
from django.views.static import serve
import urllib2
from django.db.models import Q







def index(request):
    return HttpResponse("hello world")


def all_cars_from_active_companies(request):
    cars_from_active = Car.objects.filter(company__is_active= True).values('company__name', 'name')
    df = pd.DataFrame(list(cars_from_active))
    response = create_file_and_serve('cars_from_active.xlsx', df)
    return response


def price_list_for_car(request, string):
    prices_for_car = PriceList.objects.filter(car__name=string).values('company__name', 'price_at_company')
    df = pd.DataFrame(list(prices_for_car))
    response = create_file_and_serve('price_list_for_car.xlsx', df)
    return response


def price_list_for_company(request, string):
    prices_for_company = PriceList.objects.filter(company__name=string).values('car__name', 'price_at_company')
    df = pd.DataFrame(list(prices_for_company))
    response = create_file_and_serve('price_list_for_company.xlsx', df)
    return response


def active_contains_yud(request):
    active_contains_yud = Company.objects.filter(Q(name__contains='י') & Q(is_active=True)).values('name')
    df = pd.DataFrame(list(active_contains_yud))
    response = create_file_and_serve('active_contain_yud.xlsx', df)
    return response


def create_file_and_serve(file_name, df):
    df.to_excel(file_name, engine='openpyxl', index= False)
    with open(file_name, "rb") as excel:
            data = excel.read()
    response = HttpResponse(data,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename={fl}'.format(fl=file_name)
    return response







